// EC2 - Jenkins  -- AMAZON-LINUX

resource "aws_instance" "jenkins-server" {
    ami = var.ami    #amazon linux
    instance_type = var.instance_type
    availability_zone = var.availability_zone
    associate_public_ip_address = var.make-public
    key_name = var.keyname
    user_data = file("userdata.sh")
    iam_instance_profile = var.iam-profile
    #security_groups = [aws_security_group.jenkins-server-sg.name]
    
    tags = {
        Name = "${var.project_Name}-Instance"
    }
}

// Security Group

resource "aws_security_group" "jenkins-server-sg" {
    name = "${var.project_Name}-sg"
    description = "Allow all traffic to Jenkins server"
    
    ingress {
        description = "ALL"
        from_port = 0 
        to_port = 0
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
     from_port   = 0
     to_port     = 0
     protocol    = "-1"
     cidr_blocks = ["0.0.0.0/0"] 
    }
}







# // EC2 - Jenkins  -- UBUNTU

# resource "aws_instance" "jenkins-server" {
#     ami = "ami-0263e4deb427da90e"
#     instance_type = "t2.micro"
#     associate_public_ip_address = true
#     key_name = "NOVA_KP"
#     user_data = file("userdata2.sh")
#     iam_instance_profile = "ec2-admin"
    
#     tags = {
#         Name = "Jenkins-Server"
#     }
# }
