// Variables

variable "ami" {
    description = "Image for EC2"
}

variable "instance_type" {
    description = "Type of EC2 instance"
}

variable "availability_zone" {
    description = "Availability zone you want this instance in"
}

variable "make-public" {
    description = "Do you want a public_ip"
}

variable "keyname" {
    description = "Name of keypair"
}

variable "iam-profile" {
    description = "Role for ec2"
}

variable "project_Name" {
    description = "Name of project"
}